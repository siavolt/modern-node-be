/* eslint import/imports-first:0  import/newline-after-import:0 */
import path     from 'path';
import bluebird from 'bluebird';
global.Promise = bluebird;

import express     from 'express';
import logger      from 'bunyan-singletone-facade';
import middlewares from './lib/middlewares';
import router      from './lib/router';
import { appPort } from './etc/config.json';
import './lib/registerValidationRules';

// Init logger
logger.init({
    directory : path.join(__dirname, 'logs'),
    name      : 'modern-node-be'
});

// Init app
const app = express();

app.use(middlewares.json);
app.use(middlewares.urlencoded);
app.use(middlewares.cors);
app.use(middlewares.multipart);
app.use('/api/v1', router);

console.log(`APP STARTING AT PORT ${appPort}`);
app.listen(appPort);

export default app;
