import express from 'express';
import routes  from './routes';

const router = express.Router();

const checkSession = routes.sessions.check;

// Actions
router.post('/actions/:id', routes.actions.submit);

// Sessions
router.post('/sessions', routes.sessions.create);

// Contacts email
router.post('/contacts', routes.contacts.send);

// Users
router.post('/users',               routes.users.create);
router.post('/users/resetPassword', routes.users.resetPassword);
router.get('/users/:id', checkSession, routes.users.show);
router.get('/users',     checkSession, routes.users.list);
router.put('/users/:id', checkSession, routes.users.update);

// Images
router.post('/images', checkSession, routes.images.create);

// News
router.post('/news',       checkSession, routes.news.create);
router.delete('/news/:id', checkSession, routes.news.delete);
router.get('/news',                      routes.news.list);
router.get('/news/:id',                  routes.news.show);
router.put('/news/:id',    checkSession, routes.news.update);

export default router;
