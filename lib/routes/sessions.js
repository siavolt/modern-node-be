import { makeServiceRunner, runService, renderPromiseAsJson } from '../expressServiceRunning';

export default {
    create : makeServiceRunner('sessions/Create', req => req.body),

    async check(req, res, next) {
        const promise = runService('sessions/Check', {
            params : { token: req.query.token }
        });

        try {
            const userData = await promise;

            /* eslint no-param-reassign: 0 */
            req.session = {
                context : {
                    userId     : userData.id,
                    userStatus : userData.status
                }
            };

            return next();
        } catch (e) {
            return renderPromiseAsJson(req, res, promise);
        }
    }
};
