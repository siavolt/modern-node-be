import { promisify } from 'bluebird';
import jwt           from 'jsonwebtoken';
import Base          from 'service-layer/Base';
import X             from 'service-layer/Exception';
import { secret }    from '../../../etc/config.json';
import mongoose      from '../../mongoose';

const User = mongoose.model('User');

const jwtVerify = promisify(jwt.verify);

export default class Check extends Base {
    static validationRules = {
        token : [ 'required' ]
    };

    async execute({ token }) {
        try {
            const userData = await jwtVerify(token, secret);

            const isValid  = await User.findOne({ _id: userData.id, status: 'ACTIVE' });

            if (!isValid) {
                throw new Error('NOT_VALID_USER');
            }

            return userData;
        } catch (e) {
            throw new X({
                code   : 'PERMISSION_DENIED',
                fields : {
                    token : 'WRONG_TOKEN'
                }
            });
        }
    }
}
