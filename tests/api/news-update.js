import { assert }  from 'chai';
import supertest   from 'supertest';
import app         from '../../app';
import TestFactory from './TestFactory';

const factory = new TestFactory();
const request = supertest.agent(app);

let token;
let newsId;

suite('News Update');

before(async () => {
    await factory.cleanup();
    await factory.setDefaultUsers();

    const news = await factory.setDefaultNews();

    newsId = news[0].id;
    token = await factory.login(request);
});

test('Positive: update news', () => {
    return request
        .put(`/api/v1/news/${newsId}?token=${token}`)
        .send({ data : {
            title       : 'Title111',
            subtitle    : 'Subtitle111',
            text        : 'Text111',
            isPublished : true
        } })
        .expect(200)
        .expect('Content-Type', /json/)
        .expect(res => {
            const data = res.body.data;

            delete data.createdAt;

            assert.deepEqual(data, {
                id          : newsId,
                title       : 'Title111',
                subtitle    : 'Subtitle111',
                text        : 'Text111',
                image       : '',
                isPublished : true
            });
        });
});

after(async () => {
    await factory.cleanup();
});
