import { assert }  from 'chai';
import supertest   from 'supertest';

import app             from '../../app';
import TestFactory     from './TestFactory';

const factory = new TestFactory();
const request = supertest.agent(app);

let token;

suite('News Create');

before(async () => {
    await factory.cleanup();
    await factory.setDefaultUsers();

    token = await factory.login(request);
});

test('Positive: create news', () => {
    return request
        .post(`/api/v1/news?token=${token}`)
        .send({ data : {
            title    : 'Title',
            subtitle : 'Subtitle',
            text     : 'Text'
        } })
        .expect(200)
        .expect('Content-Type', /json/)
        .expect(res => {
            assert.ok(res.body.status);

            const { data } = res.body;

            delete data.createdAt;
            delete data.id;

            assert.deepEqual(data, {
                title       : 'Title',
                subtitle    : 'Subtitle',
                text        : 'Text',
                image       : '',
                isPublished : false
            });
        });
});

after(async () => {
    await factory.cleanup();
});
